//
//  Card.swift
//  HIDSniffClient
//
//  Created by Eric Betts on 1/30/21.
//

import Foundation
import CoreLocation
import GRDB

class Card : NSObject, Identifiable, FetchableRecord, PersistableRecord, CLLocationManagerDelegate {
    let locationManager = CLLocationManager()

    let bits : UInt8
    let wiegand : Data

    /*
     TODO: Re-add as derived properties
    let fc : UInt8
    let cn : UInt16
     */
    public var lastSeen : Date {
        get {
            return location.timestamp
        }
    }
    var location : CLLocation = CLLocation()

    public override var description: String {
        get {
            return "bits: \(bits)\twiegand: \(wiegand.hexDescription) @ \(location.coordinate) / \(lastSeen)";
        }
    }

    init(_ data: Data) {
        bits = data[0]
        let bytes = Int((bits + 7) / 8)
        wiegand = data.subdata(in: 1..<bytes+1)

        super.init()
        locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters // kCLLocationAccuracyBestForNavigation // or kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }

    convenience init(_ data: NSData) {
        self.init(data as Data)
    }

    required init(row: Row) {
        wiegand = row["wiegand"]
        bits = row["bits"]
        let coordinate = CLLocationCoordinate2D(
            latitude: row["latitude"],
            longitude: row["longitude"]
        )
        let lastSeen : Date = row["last_seen"]
        location = CLLocation(coordinate: coordinate, altitude: 0, horizontalAccuracy: 0, verticalAccuracy: 0, timestamp: lastSeen)
    }

    func encode(to container: inout PersistenceContainer) {
        // container["id"] = id
        container["wiegand"] = wiegand
        container["bits"] = bits
        container["latitude"] = location.coordinate.latitude
        container["longitude"] = location.coordinate.longitude
        container["last_seen"] = location.timestamp
    }

    static func createTable(dbQueue : DatabaseQueue) {
        print("createTable")
        do {
            try dbQueue.write { db in
                try db.create(table: "card", ifNotExists: true) { t in
                    t.autoIncrementedPrimaryKey("id")
                    t.column("bits", .integer).notNull()
                    t.column("wiegand", .blob).notNull()
                    t.column("last_seen", .datetime).notNull()
                    t.column("latitude", .double).notNull()
                    t.column("longitude", .double).notNull()
                }
                //try Card.deleteAll(db)
            }
        } catch {
            print("Could not create table")
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        self.location = location
        manager.stopUpdatingLocation()
    }
}
