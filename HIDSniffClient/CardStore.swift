//
//  CardStore.swift
//  HIDSniffClient
//
//  Created by Eric Betts on 1/30/21.
//

import Foundation
import SwiftUI
import CoreBluetooth
import CoreLocation
import MapKit
import GRDB
import AVFoundation

let SERVICE_DATA_LENGTH = 10

class CardStore : NSObject, ObservableObject, CBCentralManagerDelegate {
    let customService = CBUUID(string: "181c")
    //0000181c-0000-1000-8000-00805f9b34fb
    //181c
    static let shared = CardStore()
    @Published private(set) var cards: [Card] = []
    @Published private(set) var lastEvent: Date = Date(timeIntervalSince1970: 0)
    @Published private(set) var state : String = ""

    var centralManager: CBCentralManager!
    var dbQueue = DatabaseQueue()

    override init() {
        super.init()
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }

    func start() {
        setupDb()
        Card.createTable(dbQueue: dbQueue)
        do {
            try dbQueue.read { db in
                cards = try Card.fetchAll(db)
            }
        } catch {
            print("Could not load cards \(error)")
        }
    }

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    func setupDb() {
        let docs = getDocumentsDirectory()
        do {
            dbQueue = try DatabaseQueue(path: docs.appendingPathComponent("credentials.sqlite").absoluteString)
        } catch {
            print("Could not set dbQueue, using inMemory verson. \(error)")
        }
        print("setupDB")
    }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
          case .unknown:
            print("central.state is .unknown")
          case .resetting:
            print("central.state is .resetting")
          case .unsupported:
            print("central.state is .unsupported")
          case .unauthorized:
            print("central.state is .unauthorized")
          case .poweredOff:
            print("central.state is .poweredOff")
          case .poweredOn:
            print("central.state is .poweredOn")
            centralManager.scanForPeripherals(withServices: [customService])//, options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
        @unknown default:
            fatalError()
        }
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String: Any], rssi RSSI: NSNumber) {
        //print(peripheral)
        guard let serviceData = advertisementData[CBAdvertisementDataServiceDataKey] as? [CBUUID:NSData] else { return }
        guard let myServiceData = serviceData[customService] else  { return }
        centralManager.stopScan()
        lastEvent = Date()

        // Handle empty
        if (myServiceData.isEqual(to: Data(repeating: 0, count: SERVICE_DATA_LENGTH))) {
            delayedScan()
        }

        if (myServiceData[0] == 0) {
            // Handle ascii
            let message = String(decoding: myServiceData.subdata(with: NSMakeRange(1, SERVICE_DATA_LENGTH - 1)), as: UTF8.self)
            if (message.lowercased() == "connected") {
                state = message
                delayedScan()
                return
            } else if (message.lowercased() == "disconnected") {
                state = message
                delayedScan()
                return
            } else {
                state = ""
            }
            return
        }


        // Add new cards to list, update existing
        let card = Card(myServiceData)
        print(card)
        var found = false
        do {
            try dbQueue.read { db in
                let count = try Card.filter(Column("bits") == card.bits).filter(Column("wiegand") == card.wiegand).fetchCount(db)
                found = count > 0
            }
        } catch {
            print("Could not search sqlite for card. \(error)")
        }

        if (!found) {
            do {
                try dbQueue.write { db in
                    try card.insert(db)
                    // haptic
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)

                }
                try dbQueue.read { db in
                    cards = try Card.fetchAll(db)
                }
            } catch {
                print("Could not insert card. \(error)")
            }
        }

        delayedScan()
    }

    func delayedScan() {
        // TODO: base this on lastEvent
        let next = DispatchTime.now() + DispatchTimeInterval.seconds(1)
        DispatchQueue.main.asyncAfter(deadline: next){
            // Look again
            self.centralManager.scanForPeripherals(withServices: [self.customService])
        }
    }
}
