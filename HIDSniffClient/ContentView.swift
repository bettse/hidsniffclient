//
//  ContentView.swift
//  HIDSniffClient
//
//  Created by Eric Betts on 1/30/21.
//

import SwiftUI
import CoreLocation
import MapKit
import AVFoundation

struct ContentView: View {
    @EnvironmentObject var cardStore: CardStore

    @State private var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(
            latitude: 45.51952661288415,
            longitude: -122.6811147500474
        ),
        span: MKCoordinateSpan(
            latitudeDelta: 0.25,
            longitudeDelta: 0.25
        )
    )

    var body: some View {
        VStack(alignment: .center) {
            Text("Last event: \(formatDate(cardStore.lastEvent))")
            if (cardStore.state != "") {
                Text(cardStore.state)
            }
            if (cardStore.cards.count > 0 ) {
                Text("Cards").font(.largeTitle).onTapGesture {
                    // Test vibration
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                }
                List(cardStore.cards, id:\.wiegand) { card in
                    VStack(alignment: .leading) {
                        HStack() {
                            Text(card.wiegand.hexDescription).font(.headline)
                        }
                        HStack {
                            Text(formatDate(card.lastSeen)).font(.subheadline)
                        }
                    }
                }
                Map(coordinateRegion: $region, showsUserLocation: true, annotationItems: cardStore.cards) { card in
                    MapPin(coordinate: card.location.coordinate) // TODO: Use MapMarker for selected card
                }
            } else {
                Text("No cards").font(.title)
                Text("Go walk around").font(.title2)
            }
        }.onAppear(perform: self.cardStore.start)
    }

    func formatDate(_ date : Date) -> String {
        if (Date(timeIntervalSince1970: 0).distance(to: date) < 1) {
            return "Never"
        }
        let dateFormatter = DateFormatter()
        if (date.distance(to: Date()) > 24 * 60 * 60) {
            dateFormatter.dateStyle = .short
            dateFormatter.timeStyle = .none
        } else {
            dateFormatter.dateStyle = .none
            dateFormatter.timeStyle = .medium
        }
        return dateFormatter.string(from: date)
    }

    func plainNumber(_ n : NSNumber) -> String {
        let numberFormatter = NumberFormatter()
        numberFormatter.locale = .current
        numberFormatter.numberStyle = .decimal
        numberFormatter.usesGroupingSeparator = false
        return numberFormatter.string(from: n) ?? n.description
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environmentObject(CardStore.shared)
    }
}
