//
//  HIDSniffClientApp.swift
//  HIDSniffClient
//
//  Created by Eric Betts on 1/30/21.
//

import SwiftUI

@main
struct HIDSniffClientApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(CardStore.shared)
        }
    }
}
